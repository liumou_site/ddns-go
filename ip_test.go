package main

import (
	"gitee.com/liumou_site/ddns-go/model"
	"testing"
)

func TestGetIPv6AddressesStartingWith24(t *testing.T) {
	got, err := model.GetIPv6AddressesStartingWith24()
	if err != nil {
		t.Errorf("GetIPv6AddressesStartingWith24() error = %v", err)
		return
	}
	for _, v := range got {
		t.Logf("GetIPv6AddressesStartingWith24() got = %v", v)
	}
}
