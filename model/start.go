package model

import (
	"fmt"
	"gitee.com/liumou_site/logger"
	"os"
	"strings"
)

func (l *Login) Start() {
	l.Url = fmt.Sprintf("%s.%s", l.Domain.Domain.Sub, l.Domain.Domain.Domain)
	l.RemoveStrNone() // 去除字符串空
	l.CheckConfig()   // 检查环境变量配置
	l.GetParseList()
	l.IPS.Debug = l.Debug
	l.IPS.GetIp()
	if l.IPS.Err != nil {
		logger.Error(l.IPS.Err.Error())
		os.Exit(2)
	}
	l.GetRecordId() // 获取记录ID
	logger.Info("IP: ", l.IPS.Ip)
	if !strings.EqualFold(l.Domain.Domain.Type, "all") {
		if l.Debug {
			logger.Debug("如果不是需要同时更新A记录和AAAA记录，则根据设置的解析类型进行更新")
		}
		if !l.Exists {
			l.Add()
		} else {
			l.Update(l.RecordId)
		}
		return
	}
	if !l.Exists {
		if l.Debug {
			logger.Debug("检测到当前记录不存在,正在添加...")
		}
		if l.IPS.Ip4Exists {
			l.GetRecordId() // 获取记录ID
			l.IPS.Ip = l.IPS.IP4
			l.Domain.Domain.Type = "A"
			l.IPS.IpType = "A"
			//l.RecordId4 = l.RecordId
			l.Domain.Domain.Sub = l.Domain.Domain.Sub4
			l.Add()
		} else {
			logger.Warn("本机无法通过IPV4连接互联网,请检查网络配置以及DNS")
		}

		if l.IPS.Ip6Exists {
			l.GetRecordId() // 获取记录ID
			//l.GetRecordIp()
			l.IPS.Ip = l.IPS.IP6
			//l.RecordId6 = l.RecordId
			l.Domain.Domain.Type = "AAAA"
			l.IPS.IpType = "AAAA"
			l.Domain.Domain.Sub = l.Domain.Domain.Sub6
			l.Add()
		} else {
			logger.Warn("本机无法通过IPV6连接互联网,请检查网络配置以及DNS")
		}

	}
	if l.IPS.Ip4Exists {
		if l.Debug {
			logger.Debug("检测到当前IPV4连接正常,正在更新解析")
		}
		l.IPS.Ip = l.IPS.IP4
		l.Domain.Domain.Sub = l.Domain.Domain.Sub4
		l.Domain.Domain.Type = "A"
		l.IPS.IpType = "A"
		l.GetRecordId() // 获取记录ID
		if l.Debug {
			logger.Debug("当前获取ID值: ", l.RecordId4)
		}
		l.Update(l.RecordId4)
	} else {
		logger.Warn("本机无法通过IPV4连接互联网,请检查网络配置以及DNS")
	}
	if l.IPS.Ip6Exists {
		if l.Debug {
			logger.Debug("检测到当前IPV6连接正常,正在更新解析")
		}
		l.IPS.Ip = l.IPS.IP6
		l.Domain.Domain.Type = "AAAA"
		l.IPS.IpType = "AAAA"
		l.Domain.Domain.Sub = l.Domain.Domain.Sub6
		l.GetRecordId() // 获取记录ID
		if l.Debug {
			logger.Debug("当前获取ID值: ", l.RecordId4)
		}
		l.Update(l.RecordId6)
	} else {
		logger.Warn("本机无法通过IPV6连接互联网,请检查网络配置以及DNS")
	}

}
