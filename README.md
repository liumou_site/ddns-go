# ddns-go

# 介绍

使用`go`语言编写的`腾讯云`域名解析管理脚本


# 使用说明

## 使用帮助信息
```shell
PS D:\code\gitee\go\ddns-go> go run .\ddns.go -h
https://liumou.site
Usage of ddns-go:
  -c string
        设置配置文件路径 (default "/opt/apps/com.liumou.ddns-tencent/config.toml")
  -debug
        显示程序运行详细日志信息
  -sec int
        下一次更新间隔时间(秒) (default 60)
  -show
        显示程序简介
  -u6 string
        设置IPV6查询接口地址 (default "http://ipv6.liumou.site:22010/api")
  -v    显示版本信息

```

## 配置参数

### 环境变量

| 环境变量名             | 说明                | 必选                  | 默认值    | 参考值                       |
|-------------------|-------------------|---------------------|--- |---------------------------|
| `UrlIp6Api`     | IPV6接口地址          | 可选 | `https://ipv6.icanhazip.com` | 备注: 接口返回数据格式必须和默认值的一致,备用：http://ipv6.liumou.site:22010/api |


### 配置文件

默认使用: `/opt/apps/com.liumou.ddns-tencent/config.toml`


```toml
[Domain]
# 设置域名，例如 liumou.site
Domain="liumou"
# 设置子域名,例如 www
Sub="www"
# 设置IPV4专属子域名，例如 ipv4
Sub4="127.0.0.1"
# 设置IPV6专属子域名，例如 ipv6
Sub6="ipv6"
# 设置备注
Remark="模板"
# 设置IP类型,ALL\A\AAAA
Type="AAAA"
[Login]
# 设置Key
Key="Keys"
# 设置ID
Id="Ids"

```

### 配置(Linux)

> Windows 平台请自行检索Windows环境变量设置

可以执行下面的命令完成模板创建
```shell
echo '[Domain]
# 设置域名，例如 liumou.site
Domain=""
# 设置子域名,例如 www
Sub=""
# 设置IPV4专属子域名，例如 ipv4
Sub4=""
# 设置IPV6专属子域名，例如 ipv6
Sub6=""
# 设置备注
Remark="Demo"
# 设置IP类型,ALL\A\AAAA
Type=""
[Login]
# 设置Key
Key=""
# 设置ID
Id=""' > config.toml
```
然后使用编辑器编辑·`config.toml`文件

```shell
nano config.toml
```

# Linux


> 假设已经下载好适合当前环境的`ddns-go`程序,并重命名为`ddns-go`,且将此程序放在`PATH`环境变量中的任意一个目录

## 通过`service`的方式管理

[下载deb](https://down.liumou.site/client/com.liumou.ddns-tencent)

### 安装-通过DEB的方式安装

下载
```shell
wget https://down.liumou.site/client/com.liumou.ddns-tencent/com.liumou.ddns-tencent_linux-amd64_latest.deb
```

安装

```shell
sudo dpkg -i com.liumou.ddns-tencent_linux-amd64_latest.deb
```

启动服务之前,请先编辑环境变量文件

```shell
vim /opt/apps/com.liumou.ddns-tencent/config.toml
```

### 安装-通过脚本的方式安装

```shell
f=install.py;rm -f $f;wget https://gitee.com/liumou_site/ddns-go/raw/master/$f&&python3 $f
```
脚本参数说明(暂停使用)
```shell
options:
  -h, --help            show this help message and exit
  --debug               Enable debug mode
  -e ENV, --env ENV     设置环境变量文件, 默认/root/.txy.env
  -domain DOMAIN, --domain DOMAIN
                        设置域名, 默认：liumou.site
  -sub SUB, --sub SUB   设置子域名,默认：www
  -key KEY, --key KEY   设置key(未传入则会进入手动输入)
  -id ID, --id ID       设置id(未传入则会进入手动输入),

```


### 状态管理

```shell
ddns-tencent-restart  # 启动服务
ddns-tencent-status # 查看服务状态
ddns-tencent-stop # 停止服务
ddns-tencent-enable # 设置开机启动
ddns-tencent-disable # 取消开机启动
ddns-tencent-log # 查看运行日志(后面50条)
```

### 查看运行日志

```shell
journalctl -u com.liumou.ddns-tencent.service  | grep ERROR
```

效果

```shell
root@ddns:~# journalctl -u com.liumou.ddns-tencent.service  | grep ERROR
Mar 27 10:10:19 ddns ddns-tencent[618]: + echo 'ERROR TXY_KEY is empty'
Mar 27 10:10:19 ddns ddns-tencent[618]: ERROR TXY_KEY is empty
Mar 27 10:10:19 ddns ddns-tencent[618]: + echo 'ERROR : Please configure the file first : /opt/apps/com.liumou.ddns-tencent/.txy.env'
Mar 27 10:10:19 ddns ddns-tencent[618]: ERROR : Please configure the file first : /opt/apps/com.liumou.ddns-tencent/.txy.env
Mar 27 10:10:19 ddns ddns-tencent[619]: + echo 'ERROR TXY_KEY is empty'
Mar 27 10:10:19 ddns ddns-tencent[619]: ERROR TXY_KEY is empty
Mar 27 10:10:19 ddns ddns-tencent[619]: + echo 'ERROR : Please configure the file first : /opt/apps/com.liumou.ddns-tencent/.txy.env'
Mar 27 10:10:19 ddns ddns-tencent[619]: ERROR : Please configure the file first : /opt/apps/com.liumou.ddns-tencent/.txy.env
Mar 27 10:10:19 ddns ddns-tencent[620]: + echo 'ERROR TXY_KEY is empty'
Mar 27 10:10:19 ddns ddns-tencent[620]: ERROR TXY_KEY is empty
Mar 27 10:10:19 ddns ddns-tencent[620]: + echo 'ERROR : Please configure the file first : /opt/apps/com.liumou.ddns-tencent/.txy.env'
Mar 27 10:10:19 ddns ddns-tencent[620]: ERROR : Please configure the file first : /opt/apps/com.liumou.ddns-tencent/.txy.env
Mar 27 10:10:19 ddns ddns-tencent[621]: + echo 'ERROR TXY_KEY is empty'
Mar 27 10:10:19 ddns ddns-tencent[621]: ERROR TXY_KEY is empty
Mar 27 10:10:19 ddns ddns-tencent[621]: + echo 'ERROR : Please configure the file first : /opt/apps/com.liumou.ddns-tencent/.txy.env'
Mar 27 10:10:19 ddns ddns-tencent[621]: ERROR : Please configure the file first : /opt/apps/com.liumou.ddns-tencent/.txy.env
Mar 27 10:10:20 ddns ddns-tencent[622]: + echo 'ERROR TXY_KEY is empty'
Mar 27 10:10:20 ddns ddns-tencent[622]: ERROR TXY_KEY is empty
Mar 27 10:10:20 ddns ddns-tencent[622]: + echo 'ERROR : Please configure the file first : /opt/apps/com.liumou.ddns-tencent/.txy.env'
Mar 27 10:10:20 ddns ddns-tencent[622]: ERROR : Please configure the file first : /opt/apps/com.liumou.ddns-tencent/.txy.env
```


## 裸程序运行

```shell
ddns-go -sec 120 &
```

> `&`表示将程序运行在后台
> `-sec 120`表示间隔120秒更新一次ip

## Windows

> 需要打开终端,然后在终端中运行，不建议直接双击`exe`文件运行


```shell
.\ddns_windows-amd64.exe 
```

```shell
PS D:\code\gitee\go\ddns-go\client> .\ddns_windows-amd64.exe 
https://liumou.site
[2024-03-12 14:10:05] [INFO] [start:16] IP:  240e:453:dd88:b03f:e93f:f86c:842a:a17f
[2024-03-12 14:10:07] [INFO] [get:77] 记录信息获取成功
[2024-03-12 14:10:08] [INFO] [update:50] 更新成功
honor.liumou.site  : 240e:453:dd88:b03f:e93f:f86c:842a:a17f
[2024-03-12 14:10:08] [INFO] [ddns:45] 本次更新完成,正在等待下次更新,当前设置等待时间:  60 秒
```

## 错误演示

```shell
l@Honor:/mnt/d/code/gitee/go/ddns-go/client$ ./ddns_linux-amd64.bin 
https://liumou.site
[2024-03-12 14:11:23] [EROR] [check:11] 请配置环境变量: TXY_DOMAIN
此变量是设置域名的值，例如：example.com
l@Honor:/mnt/d/code/gitee/go/ddns-go/client$ 
```