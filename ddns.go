package main

import (
	"flag"
	"fmt"
	"gitee.com/liumou_site/ddns-go/model"
	"gitee.com/liumou_site/logger"
	"os"
	"time"
)

var (
	debug  bool   // debug
	show   bool   // 显示简介
	ver    bool   // 显示版本信息
	Sec    int    // 更新间隔时间(秒),默认: 60
	IP6Url string // 设置
	Config string // 设置配置文件路径
)

func main() {
	fmt.Println("https://liumou.site")
	version := "3.1"
	IP6Url = os.Getenv("UrlIp6Api")
	if len(IP6Url) <= 10 {
		IP6Url = "http://ipv6.liumou.site:22010/api"
	}
	file := "/opt/apps/com.liumou.ddns-tencent/config.toml"
	gfs := flag.NewFlagSet(fmt.Sprintf("ddns-go"), flag.ExitOnError)
	gfs.StringVar(&Config, "c", file, "设置配置文件路径")
	gfs.IntVar(&Sec, "sec", 60, "下一次更新间隔时间(秒)")
	gfs.StringVar(&IP6Url, "u6", IP6Url, "设置IPV6查询接口地址")
	gfs.BoolVar(&debug, "debug", false, "显示程序运行详细日志信息")
	gfs.BoolVar(&show, "show", false, "显示程序简介")
	gfs.BoolVar(&ver, "v", false, "显示版本信息")
	args := os.Args
	err := gfs.Parse(args[1:])
	if err != nil {
		return
	}
	if show {
		fmt.Println("这是一个腾讯云平台自动化更新域名的程序,支持IPV6/IPV4")
		os.Exit(0)
	}
	if ver {
		fmt.Println(version)
		os.Exit(0)
	}
	for {
		l := model.NewLogin()
		l.ConfigFile = Config
		l.ConfigGet()
		l.Debug = debug
		l.IPS.Debug = debug
		l.IPS.IpType = l.Domain.Domain.Type
		l.IPS.UrlIp6Api = IP6Url
		l.Start()
		logger.Info("本次更新完成,正在等待下次更新,当前设置等待时间: ", Sec, "秒")
		time.Sleep(time.Duration(Sec) * time.Second)
	}

}
