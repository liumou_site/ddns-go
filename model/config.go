package model

import (
	"fmt"
	"gitee.com/liumou_site/gf"
	"gitee.com/liumou_site/logger"
	"github.com/BurntSushi/toml"
	"os"
)

func (l *Login) ConfigGet() {
	f := gf.NewFile(l.ConfigFile)
	f.IsFile()
	if !f.IsFiles {
		logger.Error(fmt.Sprintf("找不到配置文件: %s", l.ConfigFile))
		os.Exit(2)
	}
	if _, err := toml.DecodeFile(l.ConfigFile, &l.Domain); err != nil {
		logger.Error("配置文件解析错误: ", err)
		os.Exit(3)
	}

}
