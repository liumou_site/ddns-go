package model

import (
	"fmt"
	"gitee.com/liumou_site/logger"
	"os"
	"strings"
)

func (l *Login) CheckConfig() {
	if len(l.Domain.Domain.Domain) <= 3 {
		fmt.Println(l.Domain)
		logger.Error("请先编辑配置文件: ", l.ConfigFile)
		fmt.Println("当前错误是设置域名的值，例如：example.com")
		os.Exit(3)
	}
	if len(l.Domain.Domain.Sub) == 0 {
		fmt.Println(l.Domain.Domain.Sub)
		logger.Error("请先编辑配置文件: ", l.ConfigFile)
		fmt.Println("当前错误是设置子域名的值，例如：www")
		os.Exit(3)
	}
	if len(l.Domain.Login.Key) == 0 {
		fmt.Println(l.Domain.Login.Key)
		logger.Error("请先编辑配置文件: ", l.ConfigFile)
		fmt.Println("当前错误是密钥对的SecretKey值，用于登录腾讯云,可从下面的链接创建/获取")
		fmt.Println("https://console.cloud.tencent.com/cam/capi")
		os.Exit(3)
	}
	if len(l.Domain.Login.Id) <= 10 {
		fmt.Println(l.Domain.Login.Id)
		logger.Error("请先编辑配置文件: ", l.ConfigFile)
		fmt.Println("当前错误是密钥对的ID值，用于登录腾讯云,可从下面的链接创建/获取")
		fmt.Println("https://console.cloud.tencent.com/cam/capi")
		os.Exit(3)
	}
	if l.Domain.Domain.Remark == "" {
		fmt.Println(l.Domain.Domain.Remark)
		logger.Error("请先编辑配置文件: ", l.ConfigFile)
		fmt.Println("当前错误是用于记录解析记录的备注信息，例如：家里的主机")
		os.Exit(3)
	}
	Type := l.Domain.Domain.Type
	if strings.EqualFold(Type, "A") || strings.EqualFold(Type, "AAAA") || strings.EqualFold(Type, "ALL") {
		l.Err = nil
		return
	} else {
		fmt.Println(Type)
		logger.Error("请先编辑配置文件: ", l.ConfigFile)
		fmt.Println("A: 设置此值则解析到IPV4地址")
		fmt.Println("AAAA: 设置此值则解析到IPV6地址")
		os.Exit(3)
	}
}
