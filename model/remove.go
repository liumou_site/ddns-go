package model

import "strings"

func (l *Login) RemoveStrNone() {
	l.Domain.Domain.Domain = strings.ReplaceAll(l.Domain.Domain.Domain, " ", "")
	l.Domain.Domain.Sub6 = strings.ReplaceAll(l.Domain.Domain.Sub6, " ", "")
	l.Domain.Domain.Sub = strings.ReplaceAll(l.Domain.Domain.Sub, " ", "")
	l.Domain.Domain.Sub4 = strings.ReplaceAll(l.Domain.Domain.Sub4, " ", "")
	l.Domain.Login.Key = strings.ReplaceAll(l.Domain.Login.Key, " ", "")
	l.Domain.Login.Id = strings.ReplaceAll(l.Domain.Login.Id, " ", "")
}
