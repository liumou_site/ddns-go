package model

import (
	"fmt"
	"net"
)

// GetIPv6AddressesStartingWith24 获取以24开头的IPv6地址
func GetIPv6AddressesStartingWith24() ([]string, error) {
	var matchingIPs []string
	addrs, err := net.InterfaceAddrs()
	if err != nil {
		return nil, err
	}

	for _, addr := range addrs {
		if ipnet, ok := addr.(*net.IPNet); ok && !ipnet.IP.IsLoopback() && ipnet.IP.To4() == nil {
			ipv6 := ipnet.IP.To16()
			if ipv6[0] == 0x24 { // 0x24 对应十进制的36，即IPv6地址的第一个字节为24
				matchingIPs = append(matchingIPs, ipv6.String())
			}
		}
	}

	if len(matchingIPs) == 0 {
		return nil, fmt.Errorf("未找到以24开头的IPv6地址")
	}

	return matchingIPs, nil
}
