module gitee.com/liumou_site/ddns-go

go 1.22.0

require (
	gitee.com/liumou_site/gcs v1.8.2
	gitee.com/liumou_site/logger v1.2.1
	github.com/BurntSushi/toml v1.3.2
	github.com/tencentcloud/tencentcloud-sdk-go/tencentcloud/common v1.0.873
	github.com/tencentcloud/tencentcloud-sdk-go/tencentcloud/dnspod v1.0.873
)

require (
	gitee.com/liumou_site/gbm v1.1.6 // indirect
	gitee.com/liumou_site/gf v1.2.15 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	github.com/spf13/cast v1.6.0 // indirect
	golang.org/x/sys v0.18.0 // indirect
)
