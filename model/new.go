package model

import "os"

func NewLogin() *Login {
	l := &Login{
		Domain: new(DomainConfig),
		Debug:  true,
		IPS: IP{
			Ip:        "",
			IpType:    os.Getenv("TXY_IP_TYPE"),
			Debug:     true,
			Pre:       "inet",
			Out:       "",
			SetV6:     false,
			SetV4:     false,
			IP4:       "",
			IP6:       "",
			UrlIp6Api: "https://ipv6.icanhazip.com",
		},
		Exists:    false,
		RecordId4: 0,
		RecordId6: 0,
		RecordId:  0,
		Pids: Pid{
			File:  "/opt/apps/com.liumou.ddns-tencent/pid",
			Write: false,
		},
	}
	if len(os.Getenv("UrlIp6Api")) >= 6 {
		l.IPS.UrlIp6Api = os.Getenv("UrlIp6Api")
	}
	return l
}
